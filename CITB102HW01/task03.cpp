#include <iostream>

using namespace std;

int main()
{
    string operation;
    char operatorSymbol;
    double firstNumber, secondNumber, result;

    cout<<"Enter operation(+/-/x/:) ";
    cin>>operation;
    cout<<" Enter first operand: ";
    cin>>firstNumber;
    cout<<" Enter second operand: ";
    cin>>secondNumber;
    operatorSymbol = operation[0];


    switch(operatorSymbol){
    case '-':
        result = firstNumber - secondNumber;
        break;
    case '+':
        result = firstNumber + secondNumber;
        break;
    case ':':
        result = firstNumber / secondNumber;
        break;
    case 'x':
        result = firstNumber * secondNumber;
        break;
    default:
        break;
    }
    cout<<firstNumber<<" "<<operatorSymbol<<" "<<secondNumber<<" = "<<result<<endl;
    return 0;
}
