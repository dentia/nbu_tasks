#include <sstream>
#include <iostream>
#include <iomanip>

using namespace std;

double checkIfValidGrade(string numberString){
    double number;
    stringstream convertor;
    try{
        convertor << numberString;
        convertor >> number;

        if(convertor.fail() || number < 2.00 || number > 6.00)
        {
            throw 10;
        }
    }catch(int e){
        cout<<"This grade is not valid in Bulgaria."<<endl;
        return 0;
    }

    return number;
}

int main()
{
    string firstGradeStr, secondGradeStr;
    double result;

    cout<<"Enter first grade: ";
    cin>>firstGradeStr;
    double firstGrade = checkIfValidGrade(firstGradeStr);
    if(!firstGrade) return 0;

    cout<<"Enter second grade: ";
    cin>>secondGradeStr;
    double secondGrade = checkIfValidGrade(secondGradeStr);
    if(!secondGrade) return 0;

    result = (firstGrade+secondGrade)/2;
    cout<<fixed<<"Your average grade is "<<setprecision(2)<<result<<endl;


    return 1;
}
