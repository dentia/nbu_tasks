#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int number;
    string comma = "";
    cout<<"Enter a number: ";
    cin >> number;

    for(int i = 1; i<=5; i++){
            if(i>1) comma = ", ";
        cout<<comma<<number<<"^"<<i<<"="<<pow(number, i);
    }
    cout<<endl;
    return 0;
}
